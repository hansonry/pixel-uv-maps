# Pixel UV Maps

UV Maps that you can use to create pixel art UV Textures. These are mostly used to scale and orient UV Maps.

## Preview

8x8 Image

![8x8 Image](doc/map8scaled.png)


16x16 Image

![16x16 Image](doc/map16scaled.png)

Everything else is just repeaded until the desired size is reached.
